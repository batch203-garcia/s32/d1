const http = require("http");

const port = 4000;

const server = http.createServer((req, res) => {
    if (req.url == "/items" && req.method == "GET") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("Data retrieved from the database.");
    }
    if (req.url == "/items" && req.method == "POST") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("Data to be sent to the database.");
    }
});

server.listen(port);

console.log(`Server is running at localhost:${port}`);