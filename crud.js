let http = require("http");

const port = 4000;

// Mock Database
let directory = [{
    name: "Brandon",
    email: "brandon@mail.com",
}, {
    name: "Jobert",
    email: "jobert@mail.com",
}];

const server = http.createServer((req, res) => {

    //When /users route is accessed with GET we will send directory list of users

    if (req.url == "/users" && req.method == "GET") {
        res.writeHead(200, { "Content-Type": "application/json" });
        res.write(JSON.stringify(directory));
        res.end();
    }

    if (req.url == "/users" && req.method == "POST") {
        //reqBody -> placeholder or data storage
        let reqBody = "";

        req.on("data", (data) => {
            console.log(data);

            reqBody += data;
            console.log(reqBody);
        });
        req.on("end", () => {
            console.log(typeof reqBody);

            reqBody = JSON.parse(reqBody);
            console.log(typeof reqBody);

            directory.push(reqBody);
            console.log(directory);

            res.writeHead(200, { "Content-Type": "application/json" });
            res.write(JSON.stringify(reqBody));
            res.end();
        });
    }

});



server.listen(port);

console.log(`Server running at localhost:${port}`);